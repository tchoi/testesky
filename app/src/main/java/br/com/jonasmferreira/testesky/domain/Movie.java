package br.com.jonasmferreira.testesky.domain;

import org.json.JSONObject;

/**
 * Created by quality on 30/08/17.
 */

public class Movie {
    public String id;
    public String title;
    public String overview;
    public String duration;
    public String release_year;
    public String cover_url;
    public String backdrops_url;

    public Movie() {
    }

    public Movie(String id, String title, String overview, String duration, String release_year, String cover_url, String backdrops_url) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.duration = duration;
        this.release_year = release_year;
        this.cover_url = cover_url;
        this.backdrops_url = backdrops_url;
    }

    public Movie(JSONObject movie){
        try {
            this.id = movie.getString("id");
            this.title = movie.getString("title");
            this.overview = movie.getString("overview");
            this.duration = movie.getString("duration");
            this.release_year = movie.getString("release_year");
            this.cover_url = movie.getString("cover_url");
            this.backdrops_url = movie.getString("backdrops_url");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", overview='" + overview + '\'' +
                ", duration='" + duration + '\'' +
                ", release_year='" + release_year + '\'' +
                ", cover_url='" + cover_url + '\'' +
                ", backdrops_url='" + backdrops_url + '\'' +
                '}';
    }
}
