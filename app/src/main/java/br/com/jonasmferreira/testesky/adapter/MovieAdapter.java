package br.com.jonasmferreira.testesky.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import br.com.jonasmferreira.testesky.R;
import br.com.jonasmferreira.testesky.domain.Movie;

/**
 * Created by quality on 30/08/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    private Context context;
    private List<Movie> movies;

    public MovieAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View repositoryView = inflater.inflate(R.layout.row_movie, parent, false);

        ViewHolder viewHolder = new ViewHolder(repositoryView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ImageLoader imageLoader = ImageLoader.getInstance();
        Movie movie = movies.get(position);
        TextView tvTitleMovie = holder.tvTitleMovie;
        ImageView ivMovie = holder.ivMovie;

        tvTitleMovie.setText(movie.title);
        imageLoader.displayImage(movie.cover_url, ivMovie);

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvTitleMovie;
        private ImageView ivMovie;
        public ViewHolder(View view) {
            super(view);

            tvTitleMovie = (TextView)view.findViewById(R.id.tvTitleMovie);
            ivMovie = (ImageView) view.findViewById(R.id.ivMovie);
        }
    }
}
