package br.com.jonasmferreira.testesky.utils;

/**
 * Created by tchoi on 30/08/17.
 */

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;

import java.io.InputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {
    public static void goTo (Context lcontext, Class<?> lcls, boolean newTask){
        Intent intent = new Intent(lcontext, lcls);
        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        lcontext.startActivity(intent);
    }


    public static String dateDB2BR(Date today){
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dt = df.format(today);
        return dt;
    }
    public static String dateDB2BR(String dt){
        if(dt==null){
            return dt;
        }

        String dtFormat =  dt;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date date = df.parse(dt);
            dtFormat = dateDB2BR(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dtFormat;
    }

    public static Date dateBR2DB(String dt){
        Date date;

        if(dt==null){
            return new Date();
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = df.parse(dt);
            return date;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getInitials(String text){
        Pattern p = Pattern.compile("((^| )[A-Za-z])");
        Matcher m = p.matcher(text);
        String initials="";
        while(m.find()){
            initials+=m.group().trim();
        }
        return initials;
    }


    public static Bitmap loadContactPhoto(ContentResolver cr, long  id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    //https://pt.stackoverflow.com/questions/91205/formatar-n%C3%BAmero-de-celular-em-textview-android
    public static String clearFormating(String phoneNumber) {
        phoneNumber = phoneNumber.replace("(", "").replace(")", "").replace("-", "")
                .replace(" ", "");
        return phoneNumber;
    }

    public static String formatPhoneNumber(String phoneNumber) {
        StringBuilder sb = new StringBuilder();
        if (phoneNumber.length() >= 5 && phoneNumber.length() < 9) {
            sb.append(phoneNumber.subSequence(0, 4));
            sb.append('-');
            sb.append(phoneNumber.subSequence(4, phoneNumber.length()));
        } else if (phoneNumber.length() == 9) {

            sb.append(phoneNumber.subSequence(0, 5));
            sb.append('-');
            sb.append(phoneNumber.subSequence(5, phoneNumber.length()));

        } else if (phoneNumber.length() == 10) {

            sb.append("(");
            sb.append(phoneNumber.subSequence(0, 2));
            sb.append(") ");
            sb.append(phoneNumber.subSequence(2, 6));
            sb.append("-");
            sb.append(phoneNumber.subSequence(6, phoneNumber.length()));

        } else if (phoneNumber.length() == 11) {
            if (phoneNumber.startsWith("0")) {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 3));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(3, 7));
                sb.append("-");
                sb.append(phoneNumber.subSequence(7, phoneNumber.length()));

            } else {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 2));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(2, 7));
                sb.append("-");
                sb.append(phoneNumber.subSequence(7, phoneNumber.length()));
            }

        } else if (phoneNumber.length() == 12) {
            if (phoneNumber.startsWith("0")) {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 3));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(3, 8));
                sb.append("-");
                sb.append(phoneNumber.subSequence(8, phoneNumber.length()));

            } else {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 2));
                sb.append(" ");
                sb.append(phoneNumber.subSequence(2, 4));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(4, 8));
                sb.append("-");
                sb.append(phoneNumber.subSequence(8, phoneNumber.length()));
            }

        } else if (phoneNumber.length() == 13) {
            if (phoneNumber.startsWith("0")) {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 3));
                sb.append(" ");
                sb.append(phoneNumber.subSequence(3, 5));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(5, 9));
                sb.append("-");
                sb.append(phoneNumber.subSequence(9, phoneNumber.length()));
            } else {
                sb.append("(");
                sb.append(phoneNumber.subSequence(0, 2));
                sb.append(" ");
                sb.append(phoneNumber.subSequence(2, 4));
                sb.append(") ");
                sb.append(phoneNumber.subSequence(4, 9));
                sb.append("-");
                sb.append(phoneNumber.subSequence(9, phoneNumber.length()));
            }

        } else if (phoneNumber.length() == 14) {
            sb.append("(");
            sb.append(phoneNumber.subSequence(0, 3));
            sb.append(" ");
            sb.append(phoneNumber.subSequence(3, 5));
            sb.append(") ");
            sb.append(phoneNumber.subSequence(5, 10));
            sb.append("-");
            sb.append(phoneNumber.subSequence(10, phoneNumber.length()));

        } else {
            sb.append(phoneNumber);
        }
        return sb.toString();
    }


    public static String formatMoney(double money){
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String moneyString = formatter.format(money);
        if (moneyString.endsWith(".00")) {
            int centsIndex = moneyString.lastIndexOf(".00");
            if (centsIndex != -1) {
                moneyString = moneyString.substring(1, centsIndex);
            }
        }

        return moneyString;
    }
}
