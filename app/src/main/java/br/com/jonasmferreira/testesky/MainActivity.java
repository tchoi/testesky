package br.com.jonasmferreira.testesky;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.jonasmferreira.testesky.adapter.MovieAdapter;
import br.com.jonasmferreira.testesky.domain.Movie;
import br.com.jonasmferreira.testesky.utils.Tools;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    public static String TAG = MainActivity.class.getSimpleName();
    private android.support.v7.widget.RecyclerView rvMovie;
    MovieAdapter adapter;
    Dialog progressDialog;
    Context context;
    List<Movie> movieList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        adapter = new MovieAdapter(context,movieList);
        loadComponents();
        loadData();
    }

    protected void loadComponents(){
        rvMovie = (RecyclerView) findViewById(R.id.rvMovie);
        //rvMovie.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,2);
        rvMovie.setLayoutManager(layoutManager);
    }

    protected void loadData(){
        rvMovie.setAdapter(adapter);
        rvMovie.setVisibility(View.INVISIBLE);
        showProgressDialog("Buscando filmes");

        final RequestParams params = new RequestParams();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setUserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        String url = "https://sky-exercise.herokuapp.com/api/Movies";
        client.get(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG,"LIst: "+response.toString());

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG,"LIst: "+response.toString());
                movieList.clear();
                try{
                    for(int i=0;i<response.length();i++){
                        JSONObject o = response.getJSONObject(i);
                        movieList.add(new Movie(o));
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                        rvMovie.setVisibility(View.VISIBLE);
                        rvMovie.post(new Runnable() {
                            @Override
                            public void run() {
                                final int curSize = adapter.getItemCount();
                                adapter.notifyItemRangeInserted(0, movieList.size());
                            }
                        });
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }


    protected void showProgressDialog(String msg) {
        showProgressDialog("Aguarde", msg);
    }

    protected void showProgressDialog(String title, String msg) {
        progressDialog =  new AwesomeProgressDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .show();

    }

    protected void closeProgressBar() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    protected void showErrors(String msg){
        showErrors("Ops",msg);
    }
    protected void showErrors(String title,String msg){
        showErrors("Ops",msg,false);
    }
    protected void showErrors(String title,String msg, final boolean isClose){
        progressDialog = new AwesomeErrorDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                .setCancelable(true).setButtonText(getString(R.string.dialog_ok_button))
                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                .setButtonText(getString(R.string.dialog_ok_button))
                .setErrorButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        if(isClose) {
                            // click
                            closeProgressBar();
                            finish();
                            Tools.goTo(context,MainActivity.class,false);
                        }
                    }
                })
                .show();

    }
    protected void showSuccess(String msg){
        showSuccess("Beleza",msg);
    }
    protected void showSuccess(String title,String msg){
        progressDialog = new AwesomeSuccessDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_success, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.dialog_yes_button))
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                    }
                })
                .show();

    }
}
